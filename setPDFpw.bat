@echo off

REM
REM This bat file is used to protect a PDF file with a password
REM
REM Tested on Windows 11
REM 



setlocal enabledelayedexpansion

set count=0
set "choice_options="

echo.
for %%A in (*.pdf) do (
    
	REM Increment %count% here so that it doesn't get incremented later
    set /a count+=1

    REM Add the file name to the options array
    set "options[!count!]=%%A"

    REM Add the new option to the list of existing options
    set choice_options=!choice_options!!count!
)

for /L %%A in (1,1,!count!) do echo [%%A]. !options[%%A]!
set /p filechoice="Enter a file to load: "
set /p pw="Enter a password: "

echo Running !options[%filechoice%]!...
"%~dp0\qpdf-10.6.3\bin\qpdf.exe" "!options[%filechoice%]!" --replace-input --encrypt %pw% %pw% 256 --
